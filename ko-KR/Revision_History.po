msgid ""
msgstr "Project-Id-Version: 0\nPOT-Creation-Date: 2015-10-01 18:01+0200\nPO-Revision-Date: 2019-05-05 13:49+0000\nLast-Translator: Jun Hyung Shin <shmishmi79@gmail.com>\nLanguage-Team: Korean <https://hosted.weblate.org/projects/debian-handbook/revision_history/ko/>\nLanguage: ko-KR\nMIME-Version: 1.0\nContent-Type: application/x-publican; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=1; plural=0;\nX-Generator: Weblate 3.7-dev\n"

msgid "Revision History"
msgstr "수정 기록"

msgid "Raphaël"
msgstr "라파엘"

msgid "Hertzog"
msgstr "허츠고그(Hertzog)"
